use serde::{Deserialize, Serialize};
use lambda_runtime::{service_fn, LambdaEvent, Error, Context};

#[derive(Deserialize, Serialize)]
struct Input {
    filtered: Vec<String>,
}

#[derive(Deserialize, Serialize)]
struct Output {
    combined: String
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let func = service_fn(check_palindromes);
    lambda_runtime::run(func).await?;
    Ok(())
}

async fn check_palindromes(event: LambdaEvent<Input>) -> Result<Output, Error> {
    let (event, _context): (Input, Context) = event.into_parts();
    let combined: String = event.filtered.join(" ");
    Ok(Output { combined })
}
