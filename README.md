# rb474-individualproject-4

## Description

The project is an example of using Step Functions in AWS to coordinate two AWS Lambdas: `filter` and `combine`. Given a text and a word to remove, the `filter` function removes every occurence of the word (capitalization does not matter) and `combine` function concatenates the array of words together back into a single text.

## Process
1. Initialise the projects for the functions
2. Write their functionality in Rust
3. Upload the binaries to the respective AWS Lambda functions
4. Create the state machine in AWS Step Functions which organizes the flow
5. Test the functionality of the setup

## Examples
![](media/success_example.png)

[Link to the video location](media/)